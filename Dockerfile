# build stage
FROM node:13-alpine as build-stage
WORKDIR /app
COPY . .
RUN npm install  
RUN npm run build
ENV NODE_ENV production


EXPOSE 3001

CMD ["npm", "run", "express"]
