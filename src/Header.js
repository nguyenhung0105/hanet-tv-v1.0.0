import React from "react";
import logo from "./logowhite.png";
import "./Header.scss";

const Header = () => {
	return (
		<div className='header'>
			<div className='header__wrapper-logo'>
				<img src={logo} alt='logo' />
			</div>
		</div>
	);
};

export default Header;
