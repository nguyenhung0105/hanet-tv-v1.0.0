import "./App.scss";
import Container from "./Container";
import Header from "./Header";
import React from "react";
import bg from "./bg-2.mp4";

function App() {
	return (
		<div className='app'>
			<video className='videoTag' autoPlay loop muted>
				<source src={bg} type='video/mp4' />
			</video>
			<Header />
			<Container />
			{/* <App1 /> */}
		</div>
	);
}

export default App;
